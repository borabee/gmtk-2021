extends Node2D
class_name Base_Map
#Signals
signal map_restart()
signal map_won()

#PackedScenes
export(PackedScene) var common_enemy : PackedScene
export(PackedScene) var fading_enemy : PackedScene
export(PackedScene) var mage_enemy   : PackedScene
export(PackedScene) var spell_scene  : PackedScene

#Fixed Childs
onready var Player : KinematicBody2D = $Player
onready var TweenNode : Tween = $Technical/Tween
onready var CameraNode : Camera2D = $Camera

#Objects List
var tower_list : Array
var house_list : Array
var path_list  : Array

#Map Parameters
export(Array, int) var common_per_path : Array = [0]
export(Array, int) var fading_per_path : Array = [0]
export(Array, int) var mage_per_path   : Array = [0]

#Attributes
var total_enemies : int = 0
var enemy_spawn_lists : Array = []
var lassoed : bool = false
var game_over : bool = false
var game_won : bool = false

func _ready():
	#Music or something idk
	Audio.BGM(3)

	randomize()
	path_list = $Paths.get_children()

	assert(path_list.size() == common_per_path.size(), "PATH LIST and COMMON LIST size mismatch")
	assert(path_list.size() == fading_per_path.size(), "PATH LIST and FADING LIST size mismatch")
	assert(path_list.size() == mage_per_path.size(), "PATH LIST and MAGE LIST size mismatch")

	for i in range(path_list.size()):
		var enemy_list := []

		for _i in range(common_per_path[i]):
			enemy_list.append(0)
			total_enemies += 1

		for _i in range(fading_per_path[i]):
			enemy_list.append(1)
			total_enemies += 1

		for _i in range(mage_per_path[i]):
			enemy_list.append(2)
			total_enemies += 1

		enemy_list.shuffle()

		enemy_spawn_lists.append(enemy_list)

	tower_list = $Towers.get_children()
	house_list = $Houses.get_children()

	for house in house_list:
		house.connect("enemy_entered", self, "lose")

	#Connecting by code becase godot keeps reconnecting it on extended scenes
	#AND THROWING ERRORS
	$GUI/HUD.connect("restart", self, "restart")
	$GUI/HUD.connect("start_wave", self, "_on_start_wave")

func _unhandled_input(event):
	if event.is_action_pressed("action_lasso"):
		if game_won:
			emit_signal("map_won")
		elif not lassoed:
			lassoed = true
			for tower in tower_list:
				tower.be_lassoed(Player)

func lose(house : Area2D):
	if not game_over:
		game_over = true
		print("YOU LOSE")
		#ZOOM DRAMATICO NA CASA QUE MORREU
		# warning-ignore:return_value_discarded
		TweenNode.remove_all()
		# warning-ignore:return_value_discarded
		TweenNode.interpolate_property(CameraNode, "position", 
										CameraNode.position, house.position, 2,
										Tween.TRANS_EXPO, Tween.EASE_OUT)
		# warning-ignore:return_value_discarded
		TweenNode.interpolate_property(CameraNode, "zoom", 
										Vector2.ONE, Vector2.ONE/3, 2,
										Tween.TRANS_EXPO, Tween.EASE_OUT)
		# warning-ignore:return_value_discarded
		TweenNode.start()

func _on_start_wave():
	$Technical/Spawn_Enemy.start()

func restart():
	# warning-ignore:return_value_discarded
	TweenNode.remove_all()
	# warning-ignore:return_value_discarded
	TweenNode.interpolate_property(self, "modulate", 
									Color.white, Color(0,0,0,0), 0.3)
	# warning-ignore:return_value_discarded
	TweenNode.interpolate_property($GUI/HUD, "modulate", 
									Color.white, Color(0,0,0,0), 0.3)
	# warning-ignore:return_value_discarded
	TweenNode.start()
	yield(TweenNode, "tween_all_completed")
	yield(get_tree().create_timer(0.3), "timeout")
	self.emit_signal("map_restart")

func _on_Spawn_Enemy_timeout():
	for i in range(enemy_spawn_lists.size()):
		if not enemy_spawn_lists[i].empty():
			var enemy : Enemy
			var enemy_type : int = enemy_spawn_lists[i].pop_back()

			match enemy_type:
				0:
					enemy = common_enemy.instance()
				1:
					enemy = fading_enemy.instance()
				2:
					enemy = mage_enemy.instance()
					# warning-ignore:return_value_discarded
					enemy.connect("spell", self, "launch_spell")

			#warning-ignore:return_value_discarded
			enemy.connect("died", self, "_on_enemy_died")

			path_list[i].call_deferred("add_child", enemy)

func launch_spell(spawn_pos : Vector2, dest : Vector2):
	var spell_instance = spell_scene.instance()
	self.add_child(spell_instance)
	
	# Get last spell
	var last_spell = self.get_child(self.get_child_count() - 1)
	last_spell.set_target(spawn_pos, dest)

func _on_enemy_died(_enemy):
	self.total_enemies -= 1
	if not game_over and self.total_enemies <= 0:
		print("YOU WIN")
		game_won = true
		$GUI/Win_Label.show()
