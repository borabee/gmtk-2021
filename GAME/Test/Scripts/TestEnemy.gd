extends Node2D

export(PackedScene) var enemy_scene

func _ready():
	var enemy_instance = enemy_scene.instance()
	
	self.add_child(enemy_instance)
	
	$Timer.start()

func _on_Timer_timeout():
	var enemy_instance = enemy_scene.instance()
	
	self.add_child(enemy_instance)
	
	$Timer.start()
