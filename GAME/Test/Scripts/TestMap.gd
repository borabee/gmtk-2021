extends Node2D

export(PackedScene) var enemy_scene

export var enemies : int = 7

func _ready():
	var enemy_instance = enemy_scene.instance()
	
	$Path2D.add_child(enemy_instance)
	
	$Timer.start()

func _on_Timer_timeout():
	enemies -= 1
	if enemies > 0:
		var enemy_instance = enemy_scene.instance()
		$Path2D.add_child(enemy_instance)
		$Timer.start()
