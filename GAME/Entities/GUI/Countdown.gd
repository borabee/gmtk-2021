extends Label

func _process(_delta):
	var time_left = $Timer.time_left
	text = str(int(time_left / 60)).pad_zeros(2) + ":" + str(int(time_left) - 60*int(time_left/60)).pad_zeros(2) + ":" + str(int(fmod(time_left, 1)*10)).pad_zeros(1)
