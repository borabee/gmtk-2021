extends Enemy

onready var tween_values = [Color(1, 1, 1, 1), Color(1, 1, 1, 0)]

func _init():
	hp = 4
	speed = 20

func _ready():
	$Tween.connect("tween_completed", self, "_on_Tween_tween_completed")
	_tween_start()

func _tween_start():
	$Tween.interpolate_property($Fading_Enemy, "modulate", tween_values[0], tween_values[1], 2, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()

func _on_Tween_tween_completed(object, key):
	tween_values.invert()
	$Fading_Enemy/CollisionShape2D.disabled = !$Fading_Enemy/CollisionShape2D.disabled
	_tween_start()

