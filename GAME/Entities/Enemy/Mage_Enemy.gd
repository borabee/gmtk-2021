extends Enemy

onready var charge_shot = true
onready var shot = false
onready var staff_degrees = [0, -45]
onready var particles_pos = [Vector2(18, -9), Vector2(10, -13)]
onready var particles_emitting = [false, true]

onready var towers = []

signal spell

func _init():
	hp = 3
	speed = 40

func _ready():
	print(towers)

func _physics_process(_delta):
	if(towers.empty()):
		$Timer.stop()

func _on_Timer_timeout():
	var staff = $KinematicBody2D/Sprite/Staff
	var particles = $KinematicBody2D/CPUParticles2D
	
	_staff_anim(staff, particles)
	
	charge_shot = !charge_shot
	$Timer.start()

func _staff_anim(staff, particles):
	if(charge_shot):
		emit_signal("spell", $KinematicBody2D/CPUParticles2D.get_global_position(), towers[0].get_global_position())
	
	staff_degrees.invert()
	particles_pos.invert()
	particles_emitting.invert()
	
	$Tween.interpolate_property(staff, "rotation_degrees", staff_degrees[0], staff_degrees[1], .5, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	$Tween.interpolate_property(particles, "position", particles_pos[0], particles_pos[1], .5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.interpolate_property(particles, "emitting", particles_emitting[0], particles_emitting[1], .5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()

func _on_Area2D_body_entered(body):
	if(body.is_in_group("tower") and !body.big_dead):
		towers.push_back(body)
		$Timer.start()

func _on_Area2D_body_exited(body):
	if(body.is_in_group("tower")):
		for t in towers:
			if(t == body):
				towers.erase(t)
