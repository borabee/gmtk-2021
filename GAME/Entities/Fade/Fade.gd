extends Node2D

signal end
var is_out = false

func _ready():
	$Tween.connect("tween_all_completed", self, "on_tween_completed")
	fade_in()

func fade_in():
	$Tween.interpolate_property(self, "modulate:a", 1, 0, 1)
	$Tween.start()

func fade_out():
	$Tween.interpolate_property(self, "modulate:a", 0, 1, 1)
	$Tween.start()
	is_out = true

func on_tween_completed():
	if is_out:
		emit_signal("end")
	call_deferred("free")
