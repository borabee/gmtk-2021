extends Node2D

func _ready():
	Audio.BGM(3)

func _on_Play_Button_button_down():
	Audio.SFX(4,0)
	get_tree().change_scene("res://Map/Map_Controller/Map_Controller.tscn")

func _on_Credits_Button_button_down():
	Audio.SFX(4,0)
	get_tree().change_scene("res://Entities/Misc/Credits.tscn")

func _on_Quit_Button_button_down():
	Audio.SFX(4,0)
	get_tree().quit()
