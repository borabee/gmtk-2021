extends Map

var actual_text : int = 0
var is_in : bool = false
var texts : Array = [
		"Use WASD to move and SPACE to link to your CRYSTALS and make them follow you.\nBut remember, once joined together you can't break it.", 
		"You can push the CRYSTALS to you clicking on them or with SPACE to push all at the same time.",
		"Once the timer ends or you press the PLAY button, the enemies will come, place your CRYSTALS, but remember they can't be too close, else they will turn off.",
]
onready var tween : Tween = $Tween

func _ready():
	tween.connect("tween_all_completed", self, "on_tween_completed")
	tween.interpolate_property($Control, "modulate:a", 0, 1, 1)
	tween.start()
	$Control/Label.text = texts[actual_text]

func _unhandled_input(event):
	if event.is_action_pressed("action_lasso"):
		if actual_text <= 1:
			tween.interpolate_property($Control/Label, "modulate:a", 1, 0, 0.5)
			tween.start()
			actual_text += 1
			is_in = true
	elif (event is InputEventMouseButton and event.pressed):
		if actual_text == 1:
			tween.interpolate_property($Control/Label, "modulate:a", 1, 0, 0.5)
			tween.start()
			actual_text += 1
			is_in = true

func start_wave():
	if actual_text < 2:
		actual_text = 2
		tween.interpolate_property($Control/Label, "modulate:a", 1, 0, 0.5)
		tween.start()
		is_in = true

func on_tween_completed():
	if actual_text != 0:
		if is_in:
			$Control/Label.text = texts[actual_text]
			tween.interpolate_property($Control/Label, "modulate:a", 0, 1, 0.5)
			tween.start()
		is_in = not is_in
