extends KinematicBody2D

onready var AnimationPlayer = $AnimationPlayer
onready var Sprite = $Sprite
const ACCEL := 0.1
var MAX_SPEED := 150
const FRICTION := 0.1

var speed := Vector2.ZERO

func _physics_process(_delta):
	var mov := Vector2.ZERO
	mov.x = Input.get_action_strength("move_right")-Input.get_action_strength("move_left")
	mov.y = Input.get_action_strength("move_down")-Input.get_action_strength("move_up")
	mov = mov.normalized()

	if mov.length() > 0:
		speed = lerp(speed, mov.normalized() * MAX_SPEED, ACCEL)
	else:
		speed = lerp(speed, Vector2.ZERO, FRICTION)

	if mov != Vector2.ZERO:
		AnimationPlayer.play("walking")
	else:
		AnimationPlayer.stop()
		Sprite.rotation_degrees = 0

	speed = self.move_and_slide(speed)
