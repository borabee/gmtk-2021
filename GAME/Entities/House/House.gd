extends Area2D

signal enemy_entered(house)

func _on_body_entered(body: Node2D):
	Audio.SFX(1)
	if body.is_in_group("enemy"):
		self.emit_signal("enemy_entered", self)
