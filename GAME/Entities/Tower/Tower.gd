extends KinematicBody2D

export(PackedScene) var SHOOT_SCENE : PackedScene

onready var reject_area := $RejectionArea
onready var sprite := $Sprite
onready var particles := $Particles
onready var shoot_area := $ShootArea/CollisionShape2D

const ACCEL := 0.1
const MAX_SPEED := 150
const FRICTION := 0.1

var lasso_distance : float
var Player : KinematicBody2D
var speed := Vector2.ZERO
var reach : float

var active := true
var intruders : int
var big_dead := false
var beign_pulled := false setget set_pulled
var current_target

var hp = 5

export var ammo : int = 0 setget set_ammo
onready var lasso := $Lasso

func _ready():
	reach = shoot_area.get_shape().get_radius()

func set_pulled(value : bool):
	beign_pulled = value
	particles.emitting = value

func set_ammo(value : int):
	ammo = value
	$Ammo.text = String(ammo)

func be_lassoed(Body : KinematicBody2D):
	Player = Body
	var dir = (Player.global_position - self.global_position)
	lasso_distance = dir.length()
	lasso.points[1] = dir
	lasso.show()

func _physics_process(_delta):
	assert_target()
	if Player != null:
		var dir = (Player.global_position - self.global_position)
		lasso.points[1] = dir

		if dir.length() < lasso_distance:
			lasso_distance = dir.length()

		#A little error factor of 1 is added in other to avoid rouding problems
		if dir.length() > lasso_distance+(1) or beign_pulled:
			speed = lerp(speed, dir.normalized() * MAX_SPEED, ACCEL)

		if dir.length() == lasso_distance:
			speed = lerp(speed, Vector2.ZERO, FRICTION)

		speed = self.move_and_slide(speed)

		#If the tower collided, allow the lasso to strech (walls)
		if self.get_slide_count() > 0:
			lasso_distance = (Player.global_position - self.global_position).length()

	if not current_target or not target_in_range():
		for remaining in $ShootArea.get_overlapping_bodies():
			if remaining.is_in_group('enemy'):
				lock_on(remaining)
				return

func assert_target():
	if not current_target:
		return
	var weak = weakref(current_target)
	if not weak.get_ref():
		current_target = null

func lock_on(target):
	if current_target == target:
		return
	current_target = target
	current_target.add_to_group('targeted')
	#print("estou targetando isso aqui ", current_target.name)

func _on_Shoot_timeout():
	if current_target != null:
		shoot()

func shoot():
	if not active: return
	if ammo > 0 and target_in_range():
		print('pew pew')
		self.ammo -= 1
		var shoot : Line2D = SHOOT_SCENE.instance()
		shoot.position.y = -24
		shoot.target = current_target
		self.call_deferred("add_child", shoot)
		Audio.SFX(0)

		if ammo == 0:
			self.die()

func target_in_range():
	if not current_target:
		return false
	#print(current_target)
	var dist = abs(global_position.distance_to(current_target.global_position))
	if dist <= reach:
		return true
	else:
		return false

func update_active():
	if intruders > 0:
		active = false
		$AnimationPlayer.stop()
		for part in sprite.get_children():
			part.set("modulate", Color.gray)
	else:
		$AnimationPlayer.play("Reactivate")
		sprite.set("modulate", Color.white)
		active = true
	
func _on_RejectionArea_body_entered(body):
	if big_dead: return
	if body.is_in_group("tower") and body != self:
		if body.big_dead: return
		if active:
			print('sai de mim piranha vou desligar beweoooooooommmmm...')
		intruders += 1
		update_active()

func _on_RejectionArea_body_exited(body):
	if big_dead: return
	if body.is_in_group("tower") and body != self:
		if body.big_dead: return
		intruders -= 1
		update_active()

func _on_ShootArea_body_entered(body):
	if body.is_in_group('enemy') and !body.is_in_group('targeted'):
		if $Shoot_Timer.is_stopped():
			$Shoot_Timer.start()
		lock_on(body)

func _on_ShootArea_body_exited(body):
	if body.is_in_group('enemy') and body.is_in_group('targeted'):
		body.remove_from_group('targeted')
		for remaining in $ShootArea.get_overlapping_bodies():
			if remaining.is_in_group('enemy') and !remaining.is_in_group('targeted'):
				lock_on(remaining)
				return

func _on_Click(_viewport, event, _shape_idx):
	if (event is InputEventMouseButton and event.pressed):
		self.beign_pulled = true

func _unhandled_input(event):
	if (event is InputEventMouseButton and not event.pressed):
		self.beign_pulled = false
	if event.is_action_pressed("action_lasso") and Player != null:
		self.beign_pulled = true
	if event.is_action_released("action_lasso"):
		self.beign_pulled = false

func take_damage(dmg):
	hp -= dmg
	if(hp == 0):
		die()

func die():
	active = false
	big_dead = true
	$Ammo.hide()
	$ShootArea/CollisionShape2D.disabled = true
	$AnimationPlayer.play("Dead")
